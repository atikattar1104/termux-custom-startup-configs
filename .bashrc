# Don't Edit The Files In The Github Repository. Instead, Create Your Github Repository And Add Your Modified Config Files To It And Share. I Would Love To See Them Too!
# Thanks To Termux, Bash, And Everyone Who Helped In Customization.
# Modified Version Of Original 'bash.bashrc' File Found In Termux. Modified By Atik S. Attar 'github.com/atikattar1104/Termux-Custom-Startup-Configs'.

# Command history tweaks:
# - Append history instead of overwriting
#   when shell exits.
# - When using history substitution, do not
#   exec command immediately.
# - Do not save to history commands starting
#   with space.
# - Do not save duplicated commands.
shopt -s histappend
shopt -s histverify
export HISTCONTROL=ignoreboth

# Option To Enable 'cd' When Entering Just A Path.
shopt -s autocd

# Option To Make Bash Save History After Every Command Executed, And Preserving Termux's Default "autojump_add_to_database" Option. Remove This Default Option When Shows "Command Not Found".
export PROMPT_COMMAND="autojump_add_to_database ; history -a ; history -n"

# Default command line prompt.

# Setting This ↓ Variable To '0' Results In Showing Full Directory Address When You Change Them Using cd Command. Default Value Is '2'.
PROMPT_DIRTRIM=0

# Method To Display Custom Username And Hostname On A Default Terminal Style Prompt :- 

# You Can Change These ↓ 2 Variable's Values As You Want To Change Username And Hostname. These Are Just For Display, And Doesn't Actually Change Device's Username And Hostname. And These Values Are Only Intended For Fun And Entertainment Purpose Only. Please Be Acknowledged.
# Add A Hash Before UNAME, HNAME Variables And The PS1 Line Which Have And Starts With UNAME And HNAME Variables To Disable This Method. Remove The Hash To Enable This Method.
# UNAME="[user]"
# HNAME="[Termux-Android-LocalHost]"
# Changing Some Stuff In This ↓ Line May Create Some Unintended Behavior. Only Edit If You Know What Are You Doing And What Outcome You Want. Edit At Your Own Risk!
# PS1='$UNAME@$HNAME \[\e[0;32m\]\w\[\e[0m\] \[\e[0;97m\]\$\[\e[0m\] '

# Method To Display Original Username And Hostname On A Default Terminal Style Prompt :- 

# Add A Hash Before This PS1 Line Which Have And Starts With '\u' And '\h' To Disable This Method. Remove The Hash To Enable This Method.
# Changing Some Stuff In This ↓ Line May Create Some Unintended Behavior. Only Edit If You Know What Are You Doing And What Outcome You Want. Edit At Your Own Risk!
# PS1='\u@\h \[\e[0;32m\]\w\[\e[0m\] \[\e[0;97m\]\$\[\e[0m\] '

# Method To Display Custom Username And Hostname On Kali Linux Style Terminal Prompt :- 
# Add A Hash Before Lines Starting From 'sym..., bar..., name..., end..., dir..., UNAME..., HNAME..., and PS1...' To Disable This Method. Remove The Hashes To Enable This Method.

# sym="㉿" #symbol of prompt
# bar_c="91" #color of bars
# name_c="97" #color of user & host
# end_c="97" #color of prompt end
# dir_c="97" #color of current directory
# UNAME="[root]"
# HNAME="[kali]"

# PS1='\[\033[0;${bar_c}m\]┌──(\[\033[1;97m\]$UNAME${sym}$HNAME\[\033[0;${bar_c}m\])-[\[\033[0;${dir_c}m\]\w\[\033[0;${bar_c}m\]]
# \[\033[0;${bar_c}m\]└─\[\033[1;${end_c}m\]\$\[\033[0m\] '

# Method To Display Original Username And Hostname On Kali Linux Style Terminal Prompt :- 
# Add A Hash Before Lines Starting From 'sym..., bar..., name..., end..., dir..., and PS1...' To Disable This Method. Remove The Hashes To Enable This Method.

sym="㉿" #symbol of prompt
bar_c="91" #color of bars
name_c="97" #color of user & host
end_c="97" #color of prompt end
dir_c="97" #color of current directory

PS1='\[\033[0;${bar_c}m\]┌──(\[\033[1;97m\]\u${sym}\h\[\033[0;${bar_c}m\])-[\[\033[0;${dir_c}m\]\w\[\033[0;${bar_c}m\]]
\[\033[0;${bar_c}m\]└─\[\033[1;${end_c}m\]\$\[\033[0m\] '

# Handles nonexistent commands.
# If user has entered command which invokes non-available
# utility, command-not-found will give a package suggestions.
if [ -x /data/data/com.termux/files/usr/libexec/termux/command-not-found ]; then
        command_not_found_handle() {
                /data/data/com.termux/files/usr/libexec/termux/command-not-found "$1"
        }
fi

[ -r /data/data/com.termux/files/usr/share/bash-completion/bash_completion ] && . /data/data/com.termux/files/usr/share/bash-completion/bash_completion

# You Can Add Other Commands Which You Want To Run On System Start And Add And Change Aliases Under This Comment As You Desire.
alias bashrc="nano /data/data/com.termux/files/home/.bashrc" # Opens .bashrc File Of Termux In Nano.
alias uu="apt update ; apt upgrade" # Updates And Upgrades Packages.
alias uuc="apt update ; apt upgrade ; apt autoremove ; apt autopurge" # Updates And Upgrades Packages And Removes Unwanted Files.
alias uu-full="apt update ; apt upgrade ; apt full-upgrade ; apt dist-upgrade" # Updates And Upgrades Everything.
alias uuc-full="apt update ; apt upgrade ; apt full-upgrade ; apt dist-upgrade ; apt autoremove ; apt autopurge" # Updates And Upgrades Everything And Removes Unwanted Files.
alias ..="cd .." # Go 1 Directory Back.
alias ....="cd ../.." # Go 2 Directories Back.
alias ......="cd ../../.." # Go 3 Directories Back.
alias ........="cd ../../../.." # Go 4 Directories Back.
alias help-all="compgen -abcdefgjksuv" # Lists Full Help.
alias help-some="compgen -abck" # Lists Some Help.
alias help-cmd="compgen -c" # Lists All Possible Commands As Output.
alias help-alias="compgen -a" # Lists All Possible Aliases As Output.
alias help-builtin="compgen -b" # Lists All Possible Built-In Keywords As Output.
alias xls="ls -1aiRZ --author --full-time" # Professional ls Using Built-In ls.
alias xls-full="ls -1aiLRZ --author --full-time" # Very Big Output!
alias xlsd="lsd -1laiRZ" # Just Like ls But COLORFULL!!! Requires LSD Installed.
alias xlsd-full="lsd -1laiLRZ" # Just Like ls But COLORFULL!!! Very Big Output! Required LSD Installed.
alias svh="history >> /data/data/com.termux/files/home/.svh.dat" # Saves History To A Specific File.
alias svh-v="nano /data/data/com.termux/files/home/.svh.dat" # Opens Saved History In Editor.
alias svh-p="cat /data/data/com.termux/files/home/.svh.dat" # Prints Saved History As Output In Terminal.
alias svh-c="shred -fvzun 50000 /data/data/com.termux/files/home/.svh.dat" # Securely Shreds The Saved History File Using "shred" Command.
alias svh-sc="history >> /data/data/com.termux/files/home/.svh.dat && history -c" # Saves History To A Specific File, And Deletes History.
alias rmf="rm -rfv" # Force Deletes A File/Directory Recursively, And Displays Deleted File's And/Or Directory's Names As Output.
alias rmi="rm -riv" # Asks Question To Confirm Deletion Of The File/Directory. If Yes, Then Deletes The File/Directory, And Displays Deleted File's And/Or Directory's Names As Output. If No, Cancels Deletion, And Displays Message For The Same.
alias rms="shred -fvzun" # Force Obfuscates (Encrypts) The File Content Given Number Of Times (User Must Provide Number Of Times Before File Name, Can Be Any Number), Then Adds Zeros At The End Of File, Then Deletes The File, Then Displays Entire Operation's Log As Output. Very Secure! Cannot Recover A File After Deletion, Even With Professional Recovery Softwares.
alias obfs="shred -fvzn" # Force Obfuscates (Encrypts) The File Content Given Number Of Times (User Must Provide Number Of Times Before File Name, Can Be Any Number), Then Adds Zeros At The End Of File, Then Deletes The File, Then Displays Entire Operation's Log As Output. Very Secure! Cannot Decrypt A File After Encryption, Even With Professional Decryption Softwares.
alias mkd="mkdir -p -v" # Makes The Directory, As Well As Parent Directories (As Needed), Displays Created Directory's Names As Output.
alias randir="mkdir -p -v ./$(cat /dev/urandom | tr -cd 'a-z0-9' | head -c 8)/$(cat /dev/urandom | tr -cd 'a-z0-9' | head -c 4)/" # Makes Directory With Random Names, As Well As Parent Directories (As Needed), Displays Created Directory's Names As Output.
alias dfa="df -ak --total" # Displays All Disk Space Statistics, With Units Set To 1024. May Need To Remove "--total" In Some Cases.
alias dfi="df -ik --total" # Displays All Disk Space Statistics As Inodes Rather Than Blocks, With Units Set To 1024. May Need To Remove "--total" In Some Cases.
alias mem-chk="free -ltws 1" # Displays Total Memory Statistics (As Wide) With RAM + Swap Memory, Low - High Possible Usage, Repeating Every Single Second.
alias cputemp="sensors | grep Core" # Displays CPU Temperature. May Not Work On Old/Some Devices.
alias myip="curl -s -m 5 https://ipleak.net/json/" # Displays Extremely Detailed Network Properties Visible To The Internet.
alias neofetch-conf="nano /data/data/com.termux/files/home/.config/neofetch/config.conf" # Opens Neofetch Config File. Requires Neofetch Installed.
alias termux-props="nano /data/data/com.termux/files/home/.termux/termux.properties" # Opens Termux Properties File.
alias termux-colors="nano /data/data/com.termux/files/home/.termux/color.properties" # Opens Termux Theme Properties File. Requires Termux:Styling App Installed.
alias cls="clear" # For The Sake Of Windows Command Prompt Users.
alias shutdown="cd ; history -c ; rm -rfv .bash_history ; clear ; logout" # Powerful Way To Shutdown Termux Session And Termux App, Leaving No Traces.
alias killme="cd ; history -c ; rm -rfv .bash_history ; clear ; kill -2 $PPID" # Powerful Way To Kill Termux Session And Termux App Using 'Interrupt (INT/SIGINT)' Exit Signal/Code, Leaving No Traces.
alias freezeme="kill -19 $PPID" # Freezes Entire Termux App, Causing Processes Inside Termux To Also Freeze.
cpufetch # Requires CPUFetch Installed.
neofetch # Requires Neofetch Installed.
