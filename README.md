# Termux Custom Configs For Custom Startup

Here Is The Custom Configs For Termux's Custom Startup Screen.

# Requirements :- 

> 1. Up-To-Date `Termux`, And Optionally Other Termux Apps..
> 2. Latest `NeoFetch`, `CPUFetch`, `lsd` Packages Installed.
> 3. Knowledge Of Linux's Basic And Directory Commands And Their Syntax.

# Setup :- 

Step 1 :- Clone This Repository Or Download All Files Except `README.md`.

Step 2 :- Go To `usr/etc/` Folder, Which Is Outside Of `home` Directory, Using `cd .. ; cd usr/etc/` Command.

Step 3 :- Rename `bash.bashrc` File To `old-bash.bashrc`. Useful In Case Customization Gives Errors. Then Come Back To `home` Directory.

Note :- Make Sure There Is No File Named `bash.bashrc` In This `usr/etc` Directory. If It Exists, Then, The New `.bashrc` File From `home` Directory Won't Work.

Step 4 :- Move New `.bashrc` To `home` Directory.

[OR]

Step 4 :- Create New Files Named `.bashrc` In `home` Directory, And Copy All Content Of Downloaded `.bashrc` File To The Newly Created `.bashrc` File.

Step 5 :- Run `neofetch` and `cpufetch` Commands At Once, If Never Used Before.

Step 6 :- Go To `.config/neofetch/` Folder, Which Is Inside Of `home` Directory, Using `cd .config/neofetch/` Command.

Step 7 :- Rename `config.conf` To `old-config.conf`. Useful In Case Customization Gives Errors.

Step 8 :- Move New `config.conf` To `.config/neofetch/`, Which Is Inside Of `home` Directory.

[OR]

Step 8 :- Create A New File Named `config.conf` File In `.config/neofetch/` Folder, Which Is Inside Of `home` Directory, And Copy All Content Of Downloaded `config.conf` To The Newly Created `config.conf` File.

Note :- If Files Already Exists With Same Name, Make Sure Not To Delete Them. Instead, Change Their Name (E.g. :- If `.bashrc` Exists, Change It To `old_.bashrc` Or Something Like That.). In Case, Customization Gives Errors, These Old Files Might Be Helpful.

Step 9 :- Make Sure To Read All Comments In All Files, In Order To Fully Customize As You Want.

> `ENJOY!👍`

# Note :- 

1) Make Sure To Read Comments Of All Files Before Use[IMP].

2) Keep Old Files As Backup[VIMP!].

3) Make Sure Termux And NeoFetch, CPUFetch Packages Are Updated And Upgraded[IMP].

4) Read Help Info Of NeoFetch And CPUFetch Using `--help` Parameter[IMP].

5) Notes For `motd` :- A) You Can Add Your Name Or Any Text You Want. Like Linux's Startup Text Which Is Shown When Booting The System. B) Note That This File `motd` Will Run First And Then `bash.bashrc` After That. C) You Can Also Create A Dynamic `motd.sh` File In `~/.termux/motd.sh`. D) If You Don't Like To Have Any Startup Text Of `motd` Or `motd.sh`, You Can Delete These File From `usr/etc/motd`, Which Is Outside Of `home` Directory, Or `~/.termux/motd.sh`.

> `Good Luck!👍`

# Special Thanks To :- 

1) Termux And It's Creators And Contributors For Creating Such A Great Linux System For Android.

2) NeoFetch And It's Creators And Contributors For Creating Such A Great Linux Package And Helping In Customization.

3) CPUFetch And It's Creators And Contributors For Creating Such A Great Linux Package And Helping Being "Cherry On The Cake!".

4) Bash And It's Creators And Contributors For Creating Such A Great And Unique Linux Terminal Shell.

5) And Everyone Who Helped In Customization.

> `Thanks!`
